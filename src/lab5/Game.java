package lab5;

public class Game {
	
	static final String [] gameNames = {"None", "Counter Game", 
		"Control Game", "Flag Game",
		"n/a"};
	
	private Dice dice;
	private int whichGame;
	private boolean isGameOver;
	
	//default constructor
	public Game() {
		this.dice = new Dice();
		this.whichGame = 0;
		this.isGameOver = false;
	}
	
	//non-default constructor, user should be careful with this
	public Game(Dice dice_in, int whichGame_in,
			boolean isGameOver_in) {
		this.dice = dice_in;
		this.whichGame = whichGame_in;
		this.isGameOver = isGameOver_in;
	}
	
	public String [] getGameNames() {
		return gameNames;
	}
	
	public Dice getDice() {
		return this.dice;
	}
	
	public int getGame() {
		return this.whichGame;
	}
	
	public boolean isGameOver() {
		return this.isGameOver;
	}
	
	public void setDice(Dice dice_in) {
		this.dice = dice_in;
	}
	
	public void setGame(int game_in) {
		this.whichGame = game_in;
	}
	
	public void setGameState(boolean gameState_in) {
		this.isGameOver = gameState_in;
	}
	
	public void controlGame() {
		System.out.println("Playing " + gameNames[1] + "...");
		int curRoll;
		for (int i = 0; i < 10; i++) {
			curRoll = this.dice.roll();
			System.out.println((i + 1)
					+ ". You rolled a " + curRoll + ".");
		}
	}
	
	public void eventGame() {
		System.out.println("Playing " + gameNames[2] + "...");
		final int sentinel = 3;
		int count = 0;
		int curRoll = this.dice.roll();
		System.out.println("This game will stop rolling the dice"
				+ " when a three is rolled.");
		while(curRoll != sentinel) {
			System.out.println((count + 1) +
					". Rolled a " + curRoll + ".");
			count++;
			curRoll = this.dice.roll();
		}
		if (count != 0)
			System.out.println("It took " + (count + 1) + " times for a three"
				+ " to be rolled.");
		else System.out.println("You rolled a 3 on the first roll.");
	}
	
	public void flagGame() {
		System.out.println("Playing " + gameNames[3] + "...");
		int sum = 0;
		boolean sumIsOverThirty = false;
		int count = 0;
		int curRoll = this.dice.roll();
		System.out.println("This game will continue rolling the dice until"
				+ " the sum of the rolls is 30 or more.");
		while(!sumIsOverThirty) {
			sum = sum + curRoll;
			System.out.println(count + 1 + ". Sum = " + sum);
			count++;
			if(sum >= 30) sumIsOverThirty = true;
			curRoll = this.dice.roll();
		}
		if (count > 1) System.out.println("It took " + count  + " times for the"
				+ " sum of the rolls to be 30 or more.");
		else System.out.println("You rolled a 30"
				+ " or more on the first turn.");
	}
	
	public void playGame() {
		switch (this.whichGame) {
			case 1: controlGame();
					break;
			case 2: eventGame();
					break;
			case 3: flagGame();
					break;
			case 4: this.isGameOver = true;
					System.out.println("Thanks for playing!");
					break;
			default: System.out.println("This should"
					+ " never print.");
					break;
		}
	}
	
	public String toString() {
		return "Using: " + this.dice.toString() +
				"\nSelection: " + gameNames[this.whichGame] +
				"\nIs game over? " + this.isGameOver;
	}
	
	
}

package lab5;

import java.util.Random;

public class Dice {
	//keep track of initiated dice
	private static int numDice;
	
	//what kind of die?
	private int numSides;
	
	//default constructor
	public Dice() {
		this.numSides = 6;
		numDice++;
	}
	
	//non-default constructor
	public Dice(int numSides_in) {
		this.numSides = numSides_in;
		numDice++;
	}
	
	//getters
	public int getNumSides() {
		return this.numSides;
	}
	
	public int getNumDice() {
		return numDice;
	}
	
	//setter
	public void setNumSides(int numSides_in) {
		this.numSides = numSides_in;
	}
	
	public String toString() {
		return this.numSides + "-sided die.";
	}
	
	public int compareTo(Dice dice_in) {
		int cmp = this.numSides > dice_in.getNumSides() ? +1 : 
			this.numSides < dice_in.getNumSides() ? -1 : 0;
		return cmp;
	}
	
	//roller
	public int roll() {
		Random random = new Random();
		return random.nextInt(this.numSides) + 1;
	}
}

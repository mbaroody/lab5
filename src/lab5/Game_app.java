package lab5;

public class Game_app {

	public static void main(String[] args) {
		
		Game game = new Game();
		Menu menu = new Menu();
		while(!game.isGameOver()) {
			menu.displayMenu();
			game.setGame(menu.getUserInput());
			game.playGame();
		}
		menu.close();

	}

}

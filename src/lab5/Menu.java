package lab5;

import java.util.InputMismatchException;
import java.util.List;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import java.lang.String;

public class Menu {
	
	private Scanner reader;
	private String pathToMenuFile;
	private int numOptions;
	
	//default constructor
	public Menu() {
		this.reader = new Scanner(System.in);
		this.pathToMenuFile = System.getProperty("user.dir") + 
				"/defaultMenu.txt";
		this.numOptions = 4;
	}
	
	//non-default constructor
	public Menu(Scanner reader_in,
			String pathToMenuFile_in, int numOptions_in) {
		System.out.println("Creating the non-default menu...");
		this.reader = reader_in;
		this.pathToMenuFile = pathToMenuFile_in;
		this.numOptions = numOptions_in;
	}
	
	public void setPathToMenu(String path_in) {
		this.pathToMenuFile = path_in;
	}
	
	public String getPathToMenu() {
		return this.pathToMenuFile;
	}
	
	public Scanner getScanner() {
		return this.reader;
	}
	
	public void displayMenu() {
		List<String> menuItems = null;
		try {
			menuItems = Files.readAllLines(Paths.get(pathToMenuFile), 
					Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.println("Not a valid file.");
			e.printStackTrace();
		}
		
		for (int i = 0; i < menuItems.size(); i++) {
			System.out.println(menuItems.get(i));
		}
	}
	
	public int getUserInput() {
		int input = 0;
		while (input < 1) {
			try {
				input = reader.nextInt();
			} catch (InputMismatchException e) {
				System.out.println("Please enter an integer.");
				reader.nextLine();
				continue;
			}
			
			if (input > numOptions || input < 1) {
				System.out.println("Please choose a valid option.");
				input = 0;
				reader.nextLine();
			}
		}
		return input;
	}
	
	public String toString() {
		return "Using scanner: " + this.reader.toString() 
				+ "\nMenu file at: " + this.pathToMenuFile 
				+ "\nNumber of menu options: " + this.numOptions; 
	}
	
	
	
	public void close() {
		reader.close();
	}
	
	
}
